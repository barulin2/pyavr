import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = driver.find_element_by_id('th').text
    ah = driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-26 15:12:01.876521
#2018-02-26 15:32:01.727836
#2018-02-26 15:50:01.652271
#2018-02-26 17:24:01.923476
#2018-02-26 18:17:01.298768
#2018-02-26 19:02:01.818322
#2018-02-26 20:17:02.184956
#2018-02-26 21:16:01.500063
#2018-02-26 22:16:01.746346
#2018-02-26 23:36:01.414692
#2018-02-27 01:22:01.466031
#2018-02-27 02:16:01.751063